# Cryptoverse - Explore the World of Cryptocurrency

![Cryptoverse](https://i.ibb.co/8gh5Jc8/image.png)

## Introduction
This is a code repository for the corresponding Cryptoverse app. 

We will create a cryptocurrency app. We're going to use React and multiple APIs powered by https://rapidapi.com.

In RapidApi used:

Coinranking Api used to show the cryptocurrency data in our app.
Bing News Search Api used to show news of cryptoverse app.
